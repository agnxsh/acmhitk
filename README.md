# ACM Student Chapter at HITK Board 2022

### Instructions for pulling and pushing

1. If you have the repository in your local machine already use the command to update the repository first
    ```
    git pull
    ```
    else clone the repository (SSH used here, you can do it with HTTPS as well)
    ```
    git clone git@gitlab.com:acmhitk.studentchapter/acm-hitk-board-2021-22.git
    ```

2. Add your changes
    ```
    git add .
    ```

3. Commit the changes
    ```
    git commit -m "Commit msg"
    ```

4. Push the changes

    ```
    git push
    ```


