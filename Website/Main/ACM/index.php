<?php

/* 
-- OFFICIAL ACM STUDENT CHAPTER HITK WEBSITE--

Version 1.0
Author: Sayantan Pal
Email: sayantan.world98@gmail.com (please mail me if you find any difficulties editing this)
Date: 6th May, 2022

-- Add new version details below--

 NOTE: 

    1.  This webpage should re-direct to the CURRENT ACM Student Chapters website.
    2.  This webpage should display a message that the website is under maintenance, if required.
        The file is under ./ACM Student Chapters/Maintenance/index.php.
    3.  Please update the website atleast once in a month.
    4.  Make sure all the SSL Certificates are valid. Run AutoSSL if required (Its in the Cpanel)  
    5.  Whoever makes changes to the code, you should add the VERSION NUMBER, your NAME, EMAIL and DATE.
    6.  The CURRENT Chapter Page should countain links to redirect to the OLD Chapter's Landing Page.

*/

// Under Maintenance Flag (true/false)
$under_maintenance = false;

if($under_maintenance) {
    // Redirect to the Maintenence Page
    header("Location: ./ACM Student Chapters/Maintenance/index.php");
} else{
    // Redirect to the current ACM Student Chapters website
    header("Location: ./ACM Student Chapters/ACM Student Chapter 2021-22/index.php");
}
?>